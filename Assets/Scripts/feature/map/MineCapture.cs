using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using core;
using core.utility;
using core.forger;
using managers;

namespace feature.map {
    public class MineCapture : MonoBehaviour {
        //Time in seconds it takes to capture well    
        public float CaptureTime;
        //Time in seconds it takes before opposite team can start capture
        [SerializeField]
        float lockTime;
        public float Radius;
        //Ratio that cap will decay e.g value of 2 is twice capturetime to decay
        public float CapDecay;
        public Team Current;

        public WellSpot spot;
            

        private Team CurrentTeam;
        private float Units;

        private float lockTimeCurrent;
        private float captureProgressTime = 0;
        public float CaptureProgress {
            get;
            private set;
        }

        public bool IsLocked {
            get { return (lockTimeCurrent > 0); }
        }

        public float LockTimeRatio {
            get { return lockTimeCurrent / lockTime; }
        }

        void Update() {
            CurrentTeam = Current;

            if (lockTimeCurrent > 0) {
                lockTimeCurrent -= Time.deltaTime;
                return;
            }

            var planarPos = Utils.PlanarPoint(transform.position);

            var enemyForgers = UnitManager.GetForgersOnTeam(Current.Opposite()).Where( unit => {
                return Utils.CollidesWith(unit.Position, unit.Radius, planarPos, Radius);
            });

            var allyForgers = UnitManager.GetForgersOnTeam(Current).Where( unit => {
                return Utils.CollidesWith(unit.Position, unit.Radius, planarPos, Radius);
            });

            var capturing = new List<Unit>( enemyForgers );

            var contesting = new List<Unit>( allyForgers );


            /*
            foreach (var forger in UnitManager.GetForgers()) {

                if (Utils.CollidesWith(forger.GetComponent<Unit>().Position, forger.GetComponent<Unit>().Radius
                                      , planarPos, Radius)) {
                    if (forger.GetComponent<Unit>().Team == CurrentTeam)
                        playerContest++;
                    else
                        players++;
                }
            }
            */

            if (capturing.Count > contesting.Count ) {
                captureProgressTime += Time.deltaTime;
                CaptureProgress += (2 / CaptureTime) * Time.deltaTime;
                if (captureProgressTime >= CaptureTime)
                    WellLock();
            } else if (capturing.Count == 0 && captureProgressTime > 0) {
                captureProgressTime -= Time.deltaTime / CapDecay;
                CaptureProgress -= (2 / CaptureTime) * Time.deltaTime / CapDecay;
            }
        }

        void WellLock() {
            if (CurrentTeam == Team.UpRight)
                Current = Team.DownLeft;
            else if (CurrentTeam == Team.DownLeft)
                Current = Team.UpRight;
            //swap player values once well is captured and reset timer
            captureProgressTime = 0;
            CaptureProgress = 0;
            lockTimeCurrent = lockTime;
        }

        #if UNITY_EDITOR 
        void OnDrawGizmosSelected () { 
            // Display the radius when selected 
            var position = transform.position;
            position.y = 0;
            Gizmos.DrawWireSphere(position, Radius * Constants.TO_UNITY_UNITS); 
            //            Utils.DrawCircle(transform.position, Radius * Constants.TO_UNITY_UNITS); 

        } 

        #endif 
    }


    public enum WellSpot {
        Top, Left, Right, Bottom
    }
}