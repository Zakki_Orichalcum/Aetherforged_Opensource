﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Wild Fire Buff", menuName = "Ability/Ember/Wild Fire Buff")]
	public class WildFireBuff : BuffDebuff
	{
		[SerializeField] float[] damage;
		[SerializeField] float range;
		[SerializeField] float width;
		[SerializeField] float powerScaling;
		private float count;
		public WildFireBuff()
		{
			OnApply = (pair, holder) =>
			{
				count = 0;
			};
			
			OnBasicAttack = (pair, holder, target, packet) =>
			{
				float totalDamage = damage[holder.GetAbilityLevelLessOne(pair.ability)] + powerScaling * holder.Power;
				
				Vector2 locationTarget = Utils.PointWithinRange(holder.Position, target.Position, range, range);

				if(count < 1)
				{
					Unit.ForEachEnemyInLine(range, width, holder.Position, locationTarget, holder.Team, (Unit enemy) =>
					{
						holder.DealDamage(enemy, new DamagePacket(DamageActionType.SkillOrAbility, holder) 
							{PhysicalDamage = totalDamage});
						holder.PlayAnimation("Ability2");
					});
					count += 1;
				}
				else
				{
					holder.PlayAnimation("BasicAttack");
				}
				return packet;
			};
		}
	}
}