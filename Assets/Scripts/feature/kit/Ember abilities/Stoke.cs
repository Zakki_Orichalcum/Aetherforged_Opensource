﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Stoke", menuName = "Ability/Ember/Stoke")]
	public class Stoke : Passive
	{
		[SerializeField] float healthThreshold;
		[SerializeField] BuffDebuff passiveBuff;
		[SerializeField] BuffDebuff innerFireReset;

		public Stoke()
		{
			// OnHealthChanged = (pair, holder, info, delta) =>
			// {
			// 	if (pair.CanTrigger && delta < 0 && holder.HPRatio <= healthThreshold)
			// 	{
			// 		holder.AddBuff(passiveBuff.WithSource(pair.Source));
			// 		holder.AddBuff(innerFireReset.WithSource(pair.Source));
			// 		holder.StartCooldown(pair);
			// 	}
			// };
		}
	}
}