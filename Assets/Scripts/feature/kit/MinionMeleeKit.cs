﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;

namespace feature.kit {
    [CreateAssetMenu(fileName = "Melee Minion Kit", menuName = "Units/Melee Minion Kit")]
    public class MinionMeleeKit : Kit {
        /*
        public override string ObsoleteName { get { return "melee minion"; } }
        */
        protected override void Initialize (Unit me)
        {
            
        }
        /*
    public override ForgerResource characterResource() {
        return new ForgerResource() {
            Name = "derp_juice",
            MinAmount = 0,
            MaxAmount = 0,
            Amount = 0,
            Color = Color.grey,
        };
    }
        */
        public override UnitStats GetBaseStats() {
                PerLevelStats = UnitStats.Stats(
                    MaxHealth: 10f,
                    AutoAttackDamage: 1f,
                    Hephisalt: 0.5f
                );
    		return UnitStats.Stats(
    			MaxHealth: 475,
    			MovementSpeed: 340,
    			AutoAttackDamage: 12,
    			BaseAttackRange: 120,
    			BaseAttackTime: 0.8f,
    			/*AttackHitTime: 0.4f,*/

                Hephisalt: 26f,
                Experience: 84
    		);
        }
    }
}