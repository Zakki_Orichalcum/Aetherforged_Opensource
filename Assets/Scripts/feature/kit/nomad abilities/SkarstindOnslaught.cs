﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using managers;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "SkarstindOnslaught", menuName = "Ability/Nomad/SkarstindOnslaught")]
	public class SkarstindOnslaught : Castable
	{
		[SerializeField] float[] damageLeap;
		[SerializeField] float rangeLeap;
		[SerializeField] float leapSpeed;
		[SerializeField] float leapAOERange;
		[SerializeField] float leapPowerScaling;
		[SerializeField] float[] damageCharge;
		[SerializeField] float rangeCharge;
		[SerializeField] float chargeSpeed;
		[SerializeField] float chargeAOERange;
		[SerializeField] float chargePowerScaling;

		public SkarstindOnslaught()
		{
			IsUltimate = true;
			
			LineTargetCast = (ability, caster, location) =>
			{
				Vector2 casterOGLocation = caster.Position;
				
				float totalDamageLeap = damageLeap[caster.GetAbilityLevelLessOne(ability)] 
					+ (leapPowerScaling * caster.Power);
				float totalDamageCharge = damageCharge[caster.GetAbilityLevelLessOne(ability)] 
					+ (chargePowerScaling * caster.Power);;

				Vector2 leapLocation = Utils.PointWithinRange(caster.Position, location, rangeLeap, 0);
				float actualLeapDistance = caster.GetDistance(leapLocation);

				caster.Dash(ability, leapLocation, leapSpeed, onStop: (dash) =>
				{
					float enemiesInRange = 0;
					caster.PlayAnimation("Ability4.Start");
					Unit.ForEachEnemyInRadius(leapAOERange, caster.Position, caster.Team, (enemy) => 
					{
						if(enemy.Kind == UnitKind.Minion)
						{
							enemiesInRange++;
						}
					});
					dash.BreakMe();

					if(enemiesInRange > 0)
					{
						Unit.ForEachEnemyInRadius(leapAOERange, caster.Position, caster.Team, (hitUnit) =>
						{
							caster.DealDamage(hitUnit, new DamagePacket(DamageActionType.SkillOrAbility, caster) 
								{PhysicalDamage = totalDamageLeap});
						});
						caster.PlayAnimation("Ability4.Land");
					}
					else
					{
						float chargeDist = rangeCharge - actualLeapDistance;
						Vector2 normCharge = caster.Position + (caster.Position - casterOGLocation).normalized;
						Vector2 chargeLocation = Utils.PointWithinRange(caster.Position, normCharge, chargeDist, chargeDist);

						caster.Dash(ability, chargeLocation, chargeSpeed, onStop: (dash2) =>
						{
							Debug.Log("Should be dashing!");
						});
					}
				});
				caster.StartCooldown(ability);
			};
		}
	}
}