﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;


namespace feature
{
	[CreateAssetMenu(fileName = "Fate Passive", menuName = "Passives/Items/Fate")]
	public class FatePassive : Passive
	{
		[SerializeField]
		BuffDebuff FateDebuff;
		public FatePassive()
		{
			Name = "Fate passive";

			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
			{
				if (!target.HasBuff(FateDebuff.WithSource(pair.Source)))
				{
					target.AddBuff(FateDebuff.WithSource(pair.Source));
				}
				return packet;
			};
		}
	}
}