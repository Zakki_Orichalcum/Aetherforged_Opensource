﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Protection Buff", menuName = "Passives/Items/Protection Buff")]
    public class ProtectionBuff : BuffDebuff
    {
        public float percent = 15f;
        public bool physicalDefence;
        public bool magicalDefence;
        // Note: accidentally European spelling. 

        public ProtectionBuff()
        {
            DynamicStats = (SourcePair pair, Unit holder, PassiveTracker data) => 
            {
                var total = holder.StacksOf(pair.PassiveParent) * percent;
                return new UnitStats
                {
                    Armor = physicalDefence ? holder.BonusStats.Resistance * total.Percent() : 0,
                    Resistance = magicalDefence ? holder.BonusStats.Armor * total.Percent() : 0
                };
            };
        }

    }
}