﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Gluttony Passive", menuName = "Passives/Items/Gluttony")]
    public class GluttonyPassive : Passive
    {
        [SerializeField]
        float GluttonyPassiveHealPercent = .15f;
        [SerializeField]
        float GluttonyPassiveAoEHealPercent = .5f;

        public GluttonyPassive()
        {
            Name = "Gluttony passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                HealPacket healPacket = new HealPacket(pair.Source, HealActionType.HealBurst);
                if (packet.Kind == DamageActionType.SkillOrAbility)
                {
                    float healPercent = 0;
                    if (packet.PrimaryTarget == target)
                    {
                        healPercent = GluttonyPassiveHealPercent;
                    }
                    else
                    {
                        healPercent = GluttonyPassiveAoEHealPercent;
                    }

                    float totalHeal = (packet.PreMitigationMagical + packet.PreMitigationPhysical + packet.PureDamage) * healPercent;
                    healPacket.HealAmount += totalHeal;
                    pair.Source.ProvideHeal(owner, healPacket);
                }
                return packet;
            };
        }
    }
}