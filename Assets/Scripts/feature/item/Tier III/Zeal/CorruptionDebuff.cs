﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Corruption Debuff", menuName = "Passives/Items/Corruption Debuff")]
    public class CorruptionDebuff : BuffDebuff
    {
        public float CorruptionImmunityDuration = 5;
        public float CorruptionDebuffDuration = 3;
        public float CorruptionSpeedBuffDuration = 2;
        public float CorruptionSpeedBuffIncrease = 20;

        [SerializeField] BuffDebuff CorruptionSpeedBuff;

        [SerializeField] BuffDebuff CorruptionImmunity;
        [SerializeField] BuffDebuff CorruptionLockout;


        public CorruptionDebuff()
        {
            Name = "Corruption Debuff";
            AlsoObsolete_Duration = (pair) =>
            {
                return CorruptionDebuffDuration;
            };
            BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) =>
            {
                //TODO: Keep an eye on this. This should make it so that the player can only proc the speed buff once per enemy (not based on the ally that applied the debuff)
                if (packet.Source.Kind == UnitKind.Forger && packet.Source.Team != owner.Team && packet.Source != pair.Source && !packet.Source.HasBuff(CorruptionLockout.WithSource(owner)))
                {
                    packet.Source.AddBuff(CorruptionSpeedBuff.WithSource(pair.Source));
                    packet.Source.AddBuff(CorruptionLockout.WithSource(owner));
                }
                return packet;
            };
        }
    }
}