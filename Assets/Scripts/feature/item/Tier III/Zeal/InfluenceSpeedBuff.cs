﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Influence Speed Buff", menuName = "Passives/Items/Influence Speed Buff")]
    public class InfluenceSpeedBuff : BuffDebuff
    {
        public int InfluenceBuffBuildingMaxStacks = 3;
        public float InfluenceSpeedBuffIncrease = 30;
        public float InfluenceSpeedBuffDuration = 2;
        public float InfluencePassiveBonusAttackDamage = .5f;
        public float InfluenceMarkDuration = 6;

        [SerializeField] BuffDebuff InfluenceBuffBuilding;
        [SerializeField] BuffDebuff InfluenceBuff;
        [SerializeField] BuffDebuff InfluenceMark;

        public InfluenceSpeedBuff()
        {
            Name = "Influence speed buff";
            AlsoObsolete_Duration = (pair) =>
            {
                return InfluenceSpeedBuffDuration;
            };
            DynamicStats = (SourcePair pair, Unit owner, PassiveTracker data) =>
            {
                return UnitStats.Stats(MovementSpeed: InfluenceSpeedBuffIncrease);
            };
        }
    }
}