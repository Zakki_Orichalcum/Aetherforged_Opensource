﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "AvaricePassive", menuName = "Passives/Items/Avarice")]
    public class AvaricePassive : Passive
    {
        public float[] AvaricePassiveCooldown = new float[] { .5f };
        public float AvaricePassiveRange = 170;
        public float AvaricePassiveHealthDamageRatio = .75f;
        public float AvaricePassiveHealthHealRatio = .05f;
        public float AvaricePassiveHealthHealRatioForgers = .1f;
        public float AvaricePassiveMaxHealRatio = .375f;




        public AvaricePassive()
        {
            Name = "Envy passive";
            Cooldowns = AvaricePassiveCooldown;
            OnTick = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTimer) =>
            {
                float totalForgersInRange = 0;
                float totalNonForgersInRange = 0;
                DamagePacket bonusPacket = new DamagePacket(DamageActionType.ItemEffect, owner, singletarget: false);
                float totalDamagePerTick = owner.MaxHealth * AvaricePassiveHealthDamageRatio * deltaTimer;
                bonusPacket.MagicalDamage += totalDamagePerTick;
                Unit.ForEachEnemyInRadius(AvaricePassiveRange, owner.Position, owner.Team, (Unit enemy) =>
                {
                    if (enemy.Kind == UnitKind.Forger)
                    {
                        totalForgersInRange++;
                    }
                    else
                    {
                        totalNonForgersInRange++;
                    }
                    owner.DealDamage(enemy, bonusPacket);
                });
                HealPacket healPacket = new HealPacket(pair.Source, HealActionType.HealBurst);
                float totalForgerHealRatio = totalForgersInRange * AvaricePassiveHealthHealRatioForgers;
                float totalNonForgerHealRatio = totalNonForgersInRange * AvaricePassiveHealthHealRatio;
                float totalRawHealRatio = totalForgerHealRatio + totalNonForgerHealRatio;
                float finalHealRatio = Mathf.Min(totalRawHealRatio, AvaricePassiveMaxHealRatio);
                float totalHeal = finalHealRatio * owner.MaxHealth;
                healPacket.HealAmount += totalHeal;
                pair.Source.ProvideHeal(owner, healPacket);
            };
        }
    }
}