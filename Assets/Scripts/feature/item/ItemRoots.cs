﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace feature
{
    [CreateAssetMenu(fileName = "New Root", menuName = "Items/Root")]
    public class ItemRoots : ScriptableObject
    {
        public Item[] TopLevelItems = new Item[0];

    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ItemRoots))]
    public class ItemRootEditor : Editor
    {
        private ReorderableList parentList;

        ItemRoots roots;

        private void OnEnable()
        {
            roots = (ItemRoots)target;
            parentList = new ReorderableList(serializedObject,
                serializedObject.FindProperty("TopLevelItems"),
                true, true, true, true);

            parentList.drawHeaderCallback = (rect) => {
                EditorGUI.LabelField(rect, "Tier I");
            };

            parentList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = parentList.serializedProperty.GetArrayElementAtIndex(index);
                //rect.y += 2;

                float spaceWidth = 5f;
                float dressWidth = 20f;
                float nameWidth = rect.width - dressWidth - spaceWidth;

                if (element.objectReferenceValue != null)
                {
                    EditorGUI.ObjectField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, new GUIContent(element.objectReferenceValue.name));
                }
                else
                {
                    EditorGUI.ObjectField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, new GUIContent("Please select item"));
                }
            };


        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            parentList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}
