﻿using core;
using core.forger;

namespace feature.kit
{
    public class RangedCarry : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 520,
                    HealthRegen: 1,
                    MovementSpeed: 370,
                    AutoAttackDamage: 58,
                    BaseAttackTime: 1.4f,
                    BaseAttackRange: Constants.RANGED_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 77,
                    HealthRegen: .12f,
                    AutoAttackDamage: 2.2f,
                    AttackSpeed: 2.4f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return 1f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .95f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .15f;
            }
        }

    }
}
