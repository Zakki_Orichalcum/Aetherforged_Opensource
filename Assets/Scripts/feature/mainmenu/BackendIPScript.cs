﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackendIPScript : MonoBehaviour {

    public GameObject BackendIPInputField;

	void Update () {
        NetworkStore.BaseUrl = BackendIPInputField.GetComponent<InputField>().text;
    }
}
