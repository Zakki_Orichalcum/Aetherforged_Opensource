﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using core;
using core.forger;

namespace controllers.ai {
    [RequireComponent(typeof(NavMeshAgent))]
    public class ForgerAI : UnitAI<Forger> {

        protected override void DoBrain ()
        {
            base.DoBrain();

            // stuff.
        }

        public void StopActions () {
            currentAction = ActionState.HoldGround;
            isAttackMoving = false;
            CancelAttackTarget();
            agent.isStopped = true;
            Destination = me.transform.position;

            nextTarget.Clear();
        }

    }
}
