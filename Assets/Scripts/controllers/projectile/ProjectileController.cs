﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using core;
using core.utility;
using managers;

public class ProjectileController : NetworkBehaviour {

    [SerializeField]
    protected object freeformTarget; //Vector3, Vector2 (Velocity), Unit, Transform, GameObject

    [SyncVar]
    public float Velocity = -1;

    public Vector2 Position {
        get { return Utils.PlanarPoint(transform.position); }
    }

    public float Radius = 5;

    [SyncVar]
    public string dressing;
    protected bool dressed;

    public bool StopWhenTargetLost = true;

    public TeamQualifier UnitMaskTeam = TeamQualifier.EnemyTeam;

    public Unit Source {
        get;
        set;
    }

    /// <summary>
    /// The callback for when the project hits it's target
    /// </summary>
    public Action<ProjectileController, Unit, Unit> OnHitTarget;  // source, target

    /// <summary>
    /// The callback for when the project hits a unit that isn't its target
    /// </summary>
    public Action<ProjectileController, Unit> OnHitUnit;

    public Action<ProjectileController> OnStop;  // self


    [SyncVar]
    [SerializeField]
    protected Vector3 targetLocation;

    [SyncVar]
    [SerializeField]
    protected Vector2 targetPosition;
    [SyncVar]
    [SerializeField]
    protected float targetRadius;


	// Use this for initialization
	void Start () {
        TargetInfo();
	}

    void Update () {
        EachTick();

        if (TargetIsLost) {
            LostTarget();
        } else {
            TargetInfo();
        }

        transform.LookAt(targetLocation, Vector3.up);
        transform.position = Vector3.MoveTowards(transform.position, targetLocation, Time.deltaTime * Velocity * Constants.TO_UNITY_UNITS);

        targetPosition = Utils.PlanarPoint(targetLocation);

        if (!dressed) {
            DressUp();
        }

        if (TargetIsLost) {
            if (StopWhenTargetLost || Utils.CollidesWith(Position, Radius, targetPosition, targetRadius)) {
                if (OnStop != null) {
                    OnStop(this);
                } else {
                    // Should default to this even if we have no OnHit action.
                    BreakMe();
                }
            }
        } else if (Utils.CollidesWith(Position, Radius, targetPosition, targetRadius)) {
            ReachedTarget();
        }

        if (OnHitUnit != null) {
            foreach(Unit unit in Unit.ListOf) {
                if (unit.TeamQualifiesAs(UnitMaskTeam, Source.Team) 
                    && Utils.CollidesWith(Position, Radius, unit.Position, unit.Radius)) {
                    OnHitUnit(this, unit);
                    
                }
            }
        }

        /*{
            var direction = (targetLocation - transform.position).normalized;
            RaycastHit hit;

            Debug.DrawRay(transform.position + Vector3.up *0.5f, direction * Velocity * Constants.TO_UNITY_UNITS);
            if (Physics.Raycast(transform.position + Vector3.up *0.5f , direction, out hit, Velocity * Constants.TO_UNITY_UNITS, LayerMask.NameToLayer("Terrain"))) {
                transform.position = Utils.NearbyPoint3(hit.point, transform.position, Radius);
                BreakMe();
            }

        }*/
    }

    public virtual void SetTarget(Unit unit, bool StopWhenUnitTargetLost = true) {
        this.StopWhenTargetLost = StopWhenUnitTargetLost;
        freeformTarget = unit;
    }

    public virtual void SetPlanar(Vector2 area) {
        freeformTarget = area;
        StopWhenTargetLost = false;
        targetPosition = area;
        targetLocation = new Vector3(area.x, 0, area.y) * Constants.TO_UNITY_UNITS;
    }

//	// Update is called once per frame
//	void Update () {
//        NetTick();
//
//        if (TargetIsLost) {
//            LostTarget();
//        } else {
//            targetLocation = getTargetLocation();
//            targetRadius = getTargetRadius();
//            targetPosition = Utils.PlanarPoint(targetLocation);
//        }
//
//        EachTick();
//
//        transform.position = Vector3.MoveTowards(transform.position, targetLocation, Time.deltaTime * Velocity * Constants.TO_UNITY_UNITS);
//
//        AfterMoveTick();
//
//        if (TargetIsLost) {
//            if (StopWhenTargetLost || Utils.CollidesWith(Position, Radius, targetPosition, targetRadius)) {
//                if (OnStop != null) {
//                    OnStop(this);
//                } else {
//                    // Should default to this even if we have no OnHit action.
//                    BreakMe();
//                }
//            }
//        } else if (Utils.CollidesWith(Position, Radius, targetPosition, targetRadius)) {
//            ReachedTarget();
//        }
//	}


    protected virtual void EachTick () {

    }


    protected virtual bool TargetIsLost {
        get { return freeformTarget == null; }

    }

    protected virtual void TargetInfo () {
        targetLocation = getTargetLocation();
        targetPosition = Utils.PlanarPoint(targetLocation);
        targetRadius = getTargetRadius();
    }

    protected virtual void DressUp () {
        if (ProjectileManager.Ready)
        {
            ProjectileManager.Instance.ApplyDressing(dressing, transform);

            dressed = (transform.childCount > 0);
        }
    }

    protected virtual void LostTarget () {

    }

    protected virtual void ReachedTarget () {
        BreakMe();
    }

    protected virtual Vector3 getTargetLocation () {
        if (freeformTarget.GetType() == typeof(Vector3)) return (Vector3)freeformTarget;
        if (freeformTarget.GetType() == typeof(Vector2)) return new Vector3(((Vector2)freeformTarget).x, 0, ((Vector2)freeformTarget).y) * Constants.TO_UNITY_UNITS;

        if (freeformTarget.GetType() == typeof(GameObject)) return ((GameObject)freeformTarget).transform.position;
        if (freeformTarget.GetType() == typeof(Transform)) return ((Transform)freeformTarget).position;
        if (freeformTarget.GetType() == typeof(Unit)) return ((Unit)freeformTarget).transform.position;

        return transform.position;
    }

    protected virtual float getTargetRadius () {

        if (freeformTarget.GetType() == typeof(GameObject)) return ((GameObject)freeformTarget).transform.lossyScale.magnitude * Constants.TO_AETHERFORGED_UNITS;
        if (freeformTarget.GetType() == typeof(Transform)) return ((Transform)freeformTarget).transform.localScale.magnitude * Constants.TO_AETHERFORGED_UNITS;
        if (freeformTarget.GetType() == typeof(Unit)) return ((Unit)freeformTarget).Radius;
        // TODO 2017 July 12 add Entity once that's a thing. 

        return 0f;
    }

    public virtual void BreakMe () {
        if (NetworkServer.active)
        {
            NetworkServer.Destroy(gameObject);
        }
    }
}
