﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using core;

namespace controllers
{
    public class DashController : MonoBehaviour {
        public bool moving;

        public bool StopOnTerrain;

        public NavMeshAgent agent;
        public Unit unit;
        public Rigidbody body;

        public float Velocity;

        public Vector2 TargetPosition {
            get;
            protected set;
        }

        Vector3 destination;

        public TeamQualifier UnitMaskTeam = TeamQualifier.EnemyTeam;

        public void SetPlanar(Vector2 incoming)
        {
            TargetPosition = incoming;
            destination = new Vector3(incoming.x, 0, incoming.y) * Constants.TO_UNITY_UNITS;
        }

        /// <summary>
        /// The callback for when the project hits a unit that isn't its target
        /// </summary>
        public Action<DashController, Unit> OnHitUnit;

        public Action<DashController> OnStop;  // self

        public void BreakMe () {

            agent.areaMask ^= 1 << NavMesh.GetAreaFromName("Chasm");

            body.isKinematic = true;

            body.detectCollisions = true;

            agent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;

            Destroy(this);
        }

        protected void WeShouldStop ()
        {
            agent.isStopped = true;

            if (OnStop != null) {
                OnStop(this);
            } else {
                BreakMe();
            }
        }


        private void OnCollisionStay(Collision collision)
        {
            if (StopOnTerrain && collision.gameObject.layer == LayerMask.NameToLayer("Terrain")) 
            {
                WeShouldStop();
            }

            var other = collision.gameObject.GetComponent<Unit>();

            if (other != null && other.TeamQualifiesAs(UnitMaskTeam, unit.Team))
            {
                if (OnHitUnit != null)
                {
                    OnHitUnit(this, other);
                }
            }
        }

        void OnTriggerStay(Collider bonk) {
            if (StopOnTerrain && bonk.gameObject.layer == LayerMask.NameToLayer("Terrain")) {
                if (OnStop != null) {
                    OnStop(this);
                } else {
                    // Should default to this even if we have no OnHit action.
                    BreakMe();
                }
            }

            var unit = bonk.gameObject.GetComponent<Unit>();

            if (unit != null && unit.TeamQualifiesAs(UnitMaskTeam, unit.Team)) {
                if (OnHitUnit != null) {
                    OnHitUnit(this, unit);
                }
            } else {

            }
        }

        private void Start()
        {
            agent.areaMask |= 1 << NavMesh.GetAreaFromName("Chasm");

            body = unit.GetComponent<Rigidbody>();

            agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;

            body.isKinematic = false;

            body.detectCollisions = false;

            agent.SetDestination(destination);
        }

        private void Update()
        {
            var distance = Vector2.Distance(unit.Position, TargetPosition);

            if (distance < Velocity *Time.deltaTime) {

                var direction = TargetPosition - unit.Position;

                var v3direction = new Vector3(direction.x, 0, direction.y).normalized;

                // finish our journey
                agent.Move(v3direction * distance * Constants.TO_UNITY_UNITS);
                //agent.Warp(destination);

                WeShouldStop();
            }
            else 
            {
                var direction = TargetPosition - unit.Position;

                var v3direction = new Vector3(direction.x, 0, direction.y).normalized;
                
                unit.transform.LookAt(new Vector3(destination.x, gameObject.transform.position.y, destination.z));

                agent.Move(v3direction * Velocity * Constants.TO_UNITY_UNITS * Time.deltaTime);
            }
        }
    }
}