﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

using managers;

namespace core.http {
    class HttpPostResults {
        public string Data;
        public HttpStatusCode Status; 
        public T FromJson<T>() {
            return JsonUtility.FromJson<T>(Data);
        }
        public override string ToString() {
            return "Status:" + Status.ToString() + ", Data: " + Data;
        }
    }
    class HttpPost {
        public object JsonModel;
        public Uri Endpoint;
        public HttpPost() {

        }

        public IEnumerator CoSend(Action<HttpPostResults> action) {
            string jsonString = JsonUtility.ToJson(JsonModel);

            var dataAsBytes = Encoding.ASCII.GetBytes(jsonString);

            Dictionary<string,string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Timeout", "600");

            var webrequest = new WWW(Endpoint.AbsoluteUri, dataAsBytes, headers);

            yield return webrequest;

            StringReader reader = new StringReader(webrequest.text);
            var responseHead = webrequest.responseHeaders;
            HttpStatusCode tentative = HttpStatusCode.NotAcceptable;    // FIXME there is no "default" http status code
            if (responseHead.ContainsKey("STATUS")) {
                string statusCode = responseHead ["STATUS"];
                string[] leaves = statusCode.Split(new char[] { ' ' });
                int numericStatusCode;
                if (int.TryParse(leaves[1], out numericStatusCode)) {
                    tentative = (HttpStatusCode)numericStatusCode;
                }
            }

            var data = reader.ReadToEnd();

            var results = new HttpPostResults() {
                Status = tentative,
                Data = data,
            };

            if(results.Data.StartsWith("\"")) {
                results.Data = results.Data.Substring(1, data.Length - 2);
            }

            if (!webrequest.isDone) {
                Debug.LogErrorFormat("Web Request is not done. Reader might not have given complete results. Data Length: {0}"
                    , webrequest.text.Length);
            }

            reader.Close();
               
            yield return null;
            // pass results into the lambda passed in.
            action(results);
        }

        public HttpPostResults Send() {

            string jsonString = JsonUtility.ToJson(JsonModel);

            var dataAsBytes = Encoding.ASCII.GetBytes(jsonString);

            Dictionary<string,string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Timeout", "600");

            var webrequest = new WWW(Endpoint.AbsoluteUri, dataAsBytes, headers);

            StringReader reader = new StringReader(webrequest.text);
            var responseHead = webrequest.responseHeaders;
            HttpStatusCode tentative = HttpStatusCode.NotAcceptable;    // FIXME there is no "default" http status code
            if (responseHead.ContainsKey("STATUS")) {
                string statusCode = responseHead ["STATUS"];
                int numericStatusCode;
                if (int.TryParse(statusCode, out numericStatusCode)) {
                    tentative = (HttpStatusCode)numericStatusCode;
                }
            }

            var results = new HttpPostResults() {
                Status = tentative,
                Data = reader.ReadToEnd()
            };

            if (!webrequest.isDone) {
                Debug.LogErrorFormat("Web Request is not done. Reader might not have given complete results. Data Length: {0}"
                    , webrequest.text.Length);
            }

            reader.Close();

            return results;
            /*
            var request = new HttpWebRequest(Endpoint);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Timeout = 600;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(dataAsBytes, 0, dataAsBytes.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());

            var results = new HttpPostResults() {
                Status = ((HttpWebResponse)response).StatusCode,
                Data = reader.ReadToEnd()
            };

            if(results.Status == HttpStatusCode.OK) {
                results.Data = results.Data.EndsWith("\"") && results.Data.StartsWith("\"") ? results.Data.Substring(1, results.Data.Length - 2) : results.Data;
            }

            reader.Close();

            return results;
            */
            //            return default(HttpPostResults);
        }
    }
}
