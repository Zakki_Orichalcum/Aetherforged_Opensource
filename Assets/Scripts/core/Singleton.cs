﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T instance;

    private void Awake()
    {
        instance = this as T;
    }

    public static bool Ready {
        get { return (instance != null); }
    }

	/**
      Returns the instance of this singleton.
   */
	public static T Instance
	{
		get
		{
			if(instance == null)
			{
				instance = (T) FindObjectOfType(typeof(T));

                if (instance == null) {
                    Debug.LogWarning("An instance of " + typeof(T) + 
                        " is needed in the scene. Attempting to make one." + 
                        " FIXME This is bad and means we haven't initialized" + 
                        " certain game states properly. ");
                    
                    var gob = GameObject.Find("_DebugKnob");
                    if (gob == null) {
                        gob = new GameObject("_DebugKnob");
                    }
                    instance = gob.AddComponent<T>();

                }

				if (instance == null) {
					Debug.LogError("An instance of " + typeof(T) + 
						" is needed in the scene, but there is none.");
				}
			}

			return instance;
		}
	}
}