
namespace core.utility
{
  public struct Pair<T0, T1>
  {
    public T0 Item0 { get; private set; }
    public T1 Item1 { get; private set; }
    
    public Pair( T0 v0, T1 v1 )
    {
      Item0 = v0;
      Item1 = v1;
    }
  }
  
  public struct Triple<T0, T1, T2>
  {
    public T0 Item0 { get; private set; }
    public T1 Item1 { get; private set; }
    public T2 Item2 { get; private set; }
    
    public Triple( T0 v0, T1 v1, T2 v2 )
    {
      Item0 = v0;
      Item1 = v1;
      Item2 = v2;
    }
  }
}
