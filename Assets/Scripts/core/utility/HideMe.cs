﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideMe : MonoBehaviour {

	float timeActive = 0;

	void Update () {
		if(gameObject.activeInHierarchy == false){
			return;
		}
		else{
			timeActive = Time.deltaTime + timeActive;
		}
		if(timeActive >= 0.8){
			gameObject.SetActive(false);
			timeActive = 0;
		}
	}
}
