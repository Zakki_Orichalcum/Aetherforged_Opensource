using System;
using UnityEngine;
namespace core.utility
{
  public struct Duration
  {
    const float TIME_THRESHOLD = 1f / 120;
    
    float start;
    float duration;

    public static Duration Infinite()
    {
      return new Duration( Mathf.Infinity );
    }

    public Duration( float duration ) 
    {
      start = GameTime.time;
      this.duration = duration;
    }

    public void Reset()
    {
      start = GameTime.time;
    }
    
    public float Seconds
    {
      get { return duration; }
    }

    public bool Finished
    {
      get { return duration - TimePassed <= TIME_THRESHOLD; }
    }

    public float TimePassed
    {
      get { return GameTime.time - start; }
    }
    
    public float Progress
    {
      get { return TimePassed / duration; }
    }
  }
}