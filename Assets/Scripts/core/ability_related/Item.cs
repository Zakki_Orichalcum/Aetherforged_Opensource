using System;
using System.Collections.Generic;
using UnityEngine;

using feature;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif


namespace core
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Items/Item")]
    public class Item : ScriptableObject
    {

        public string Name;
        public int Tier;
        public int Cost;
        public Sprite icon;
        [Obsolete]
        public string[] Parent = new string[1];
        public Item[] BuildsFrom = new Item[1];

        /// <summary>
        /// Gets a *lot* of parts. Only does two layers, but won't encounter an infinite loop.
        /// </summary>
        /// <value>All parts.</value>
        public HashSet<Item> AllParts 
        {
            get {
                HashSet<Item> allParts = new HashSet<Item>();

                foreach (var item in BuildsFrom)
                {
                    if (item != null)
                    {
                        allParts.Add(item);
                        allParts.UnionWith(item.BuildsFrom);
                    }
                }

                Debug.LogFormat("Item: {1} parts: {0}", allParts.Count, Name);

                return allParts;


            }
        }
        [Obsolete]
        public string[] Child = new string[2];
        public Item[] BuildsInto = new Item[0];
        public string PassiveName = "";
        public string PassiveDesc = "";
        public bool isFirstInstance = true;
        public int additionalStacks = 0;

        public List<StatType> ListStats = new List<StatType>();

        public UnitStats StaticItemStats;

        public UnitStats PercentItemStats;

        [Obsolete("Transition to StaticItemStats")]
        public Func<Unit, UnitStats> ItemStats;

        [Obsolete]
        public Func<Unit, UnitStats> PercentItemStats_Obsolete;

        public Passive UniquePassive;

        public Passive StackablePassive;


    }


#if UNITY_EDITOR
    [CustomEditor(typeof(Item))]
    public class ItemEditor : Editor
    {
        private ReorderableList parentList;
        private ReorderableList childList;
        private ReorderableList stattypesList;

        static float detailWidth = 80f;
        static float shortLabelWidth = 50f;
        static float bigIconWidth = 256f;

        static bool editName;
        static bool showDetails;
        static bool showHierarchy;
        static bool showStats;
        static bool showStatList;
        static bool showPassives;

        Item item;

        private void OnEnable()
        {
            item = (Item)target;

            // Add this item to the child list of its parent(s)
            Deorphan(item);

            parentList = new ReorderableList(serializedObject,
                serializedObject.FindProperty("BuildsFrom"),
                true, true, true, true);
            childList = new ReorderableList(serializedObject,
                                            serializedObject.FindProperty("BuildsInto"),
                true, true, true, true);
            stattypesList = new ReorderableList(serializedObject,
                serializedObject.FindProperty("ListStats"),
                true, true, true, true);
            
            
            parentList.drawHeaderCallback = (rect) => {
                EditorGUI.LabelField(rect, "Parent");
            };
            childList.drawHeaderCallback = (rect) => {
                EditorGUI.LabelField(rect, "Children");
            };
            stattypesList.drawHeaderCallback = (rect) => {
                EditorGUI.LabelField(rect, "List Stats");
            };

            parentList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = parentList.serializedProperty.GetArrayElementAtIndex(index);
                //rect.y += 2;

                float spaceWidth = 5f;
                float dressWidth = 20f;
                float nameWidth = rect.width - dressWidth - spaceWidth;


                {
                    var saveColor = GUI.color;
                    string tooltip = "";
                    string label = "";


                    if (element.objectReferenceValue == null)
                    {
                        if (item.Tier > 1)
                        {
                            GUI.color = Color.red;
                            tooltip = "Non tier 1 item needs to lead from another item";
                            label = "Please set parent";
                        }
                        else 
                        {
                            label = "Empty";
                        }

                    }
                    else
                    {
                        if (item.Tier <= 1)
                        {
                            GUI.color = Color.red;
                            tooltip = "Tier 1 items are usually starter items";
                        }

                        label = element.objectReferenceValue.name;
                    }

                    EditorGUI.ObjectField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, new GUIContent(label, tooltip));

                    GUI.color = saveColor;
                }

            };

            /*
            parentList.onChangedCallback = (list) => {
                item.Parent = new string[list.count];
                for (int rebuild = 0; rebuild < list.count; rebuild++)
                {
                    var objectMaybe = list.serializedProperty.GetArrayElementAtIndex(rebuild).objectReferenceValue;
                    if (objectMaybe != null)
                    {
                        item.Parent[rebuild] = objectMaybe.name;
                    }
                }
            };
            */

            childList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = childList.serializedProperty.GetArrayElementAtIndex(index);
                //rect.y += 2;

                float spaceWidth = 5f;
                float dressWidth = 20f;
                float nameWidth = rect.width - dressWidth - spaceWidth;



                {
                    var saveColor = GUI.color;
                    string tooltip = "";
                    string label = "";


                    if (element.objectReferenceValue == null)
                    {
                        GUI.color = Color.red;

                        label = "Empty";
                    }
                    else
                    {
                        label = element.objectReferenceValue.name;
                    }

                    if (!ParentalCheck(item, index))
                    {
                        GUI.color = Color.red;

                        tooltip = string.Format("Child does not list {0} as its parent.", item.Name);
                    }


                    EditorGUI.ObjectField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, new GUIContent(label, tooltip));
                    
                    GUI.color = saveColor;
                }



            };
            stattypesList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                rect.y += 2;

                float spaceWidth = 5f;
                float dressWidth = 140f;
                float nameWidth = rect.width - dressWidth - spaceWidth;

                item.ListStats[index] = (StatType)EditorGUI.EnumPopup(
                    new Rect(rect.x, rect.y, rect.width, rect.height),
                    item.ListStats[index]);

            };
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (editName)
            {
                EditorGUILayout.BeginHorizontal();
                editName = EditorGUILayout.Foldout(editName, string.Format("Name"));
                var nameInput = EditorGUILayout.TextField(item.Name);
                if (item.Name != nameInput)
                {
                    item.Name = nameInput;
                    EditorUtility.SetDirty(item);
                }
                EditorGUILayout.EndHorizontal();

                if (GUILayout.Button("Name from name"))
                {
                    item.Name = item.name;
                }
            }
            else 
            {
                EditorGUILayout.BeginHorizontal();
                editName = EditorGUILayout.Foldout(editName, string.Format("Name"));
                EditorGUILayout.LabelField(item.Name);
                EditorGUILayout.EndHorizontal();
            }

            if (showDetails)
            {
                //showDetails = EditorGUILayout.Foldout(showDetails, "Collapse Details");
                //EditorGUILayout.BeginVertical(GUILayout.MaxWidth(80f));

                EditorGUILayout.BeginHorizontal();
                showDetails = EditorGUILayout.Foldout(showDetails, "Tier");
                //EditorGUILayout.LabelField("Tier", GUILayout.Width(shortLabelWidth));
                item.Tier = EditorGUILayout.IntField(item.Tier, GUILayout.Width(detailWidth));
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Cost", GUILayout.Width(shortLabelWidth));
                item.Cost = EditorGUILayout.IntField(item.Cost, GUILayout.Width(detailWidth));
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();
                //EditorGUILayout.LabelField("Icon");
                item.icon = (Sprite)EditorGUILayout.ObjectField(item.icon, typeof(Sprite), false, GUILayout.Width(bigIconWidth), GUILayout.Height(bigIconWidth));

                EditorUtility.SetDirty(item);


                //EditorGUILayout.EndVertical();
            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                showDetails = EditorGUILayout.Foldout(showDetails, string.Format("Tier {0}  Cost {1}", item.Tier, item.Cost));
                item.icon = (Sprite)EditorGUILayout.ObjectField(item.icon, typeof(Sprite), false, GUILayout.Width(detailWidth), GUILayout.Height(detailWidth));
                EditorGUILayout.EndHorizontal();
            }



            //EditorGUILayout.BeginHorizontal();


            if (showHierarchy)
            {
                showHierarchy = EditorGUILayout.Foldout(showHierarchy, "Collapse Hierarchy");
                //EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));


                parentList.DoLayoutList();

                childList.DoLayoutList();
                //item.icon = (Sprite)EditorGUILayout.ObjectField(item.icon, typeof(Sprite), false, GUILayout.Width(65f), GUILayout.Height(65f));

                //EditorGUILayout.EndVertical();

                // Also add this item to the child list of its parent(s)
                Deorphan(item);
            }
            else 
            {
                string parentString = "";
                string childString = "";

                foreach(var parentItem in item.BuildsFrom)
                {
                    if (parentItem != null && parentItem.Name != "")
                    {
                        parentString += ", " + parentItem.Name;
                    }
                }
                if (parentString.Length > 1)
                {
                    parentString = parentString.Substring(2);
                }
                foreach(var childItem in item.BuildsInto)
                {
                    if (childItem != null && childItem.Name != "")
                    {
                        childString += ", " + childItem.name;
                    }
                }
                if (childString.Length > 1)
                {
                    childString = childString.Substring(2);
                }

                if (parentString != "")
                {
                    showHierarchy = EditorGUILayout.Foldout(showHierarchy, string.Format("Descends from {0}. ", parentString));
                }
                else 
                {
                    showHierarchy = EditorGUILayout.Foldout(showHierarchy, "Edit Hierarchy");
                }
                if (childString != "")
                {
                    EditorGUILayout.LabelField(string.Format("Leads to {0}.", childString));
                }

            }
            //EditorGUILayout.EndHorizontal();





            EditorGUILayout.BeginHorizontal();
            showStats = EditorGUILayout.Foldout(showStats, "Stats");
            showStatList = EditorGUILayout.Foldout(showStatList, "Select");


            EditorGUILayout.EndHorizontal();

            if (showStatList)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.Space();
                EditorGUILayout.BeginVertical();
                stattypesList.DoLayoutList();
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
            }

            if (showStats)
            {
                
                EditorGUILayout.BeginVertical();
                StatInspector(item.StaticItemStats, item.PercentItemStats, item.ListStats);
                if(item.ListStats.Count > 0)
                {
                    EditorUtility.SetDirty(item);
                }
                EditorGUILayout.EndVertical();

            }
            else
            {
                
                EditorGUILayout.BeginVertical();
                StatLabelsInspector(item.StaticItemStats, item.PercentItemStats, item.ListStats);
                EditorGUILayout.EndVertical();

            }



            showPassives = EditorGUILayout.Foldout(showPassives, "Passives");
            if (showPassives)
            {
                item.UniquePassive = (Passive)EditorGUILayout.ObjectField("Unique Passive", item.UniquePassive, typeof(Passive), false);
                item.StackablePassive = (Passive)EditorGUILayout.ObjectField("Stackable Passive", item.StackablePassive, typeof(Passive), false);
                item.PassiveName = EditorGUILayout.TextField("Passive Name", item.PassiveName);
                EditorGUILayout.LabelField("Passive Description");
                item.PassiveDesc = EditorGUILayout.TextArea(item.PassiveDesc);
                EditorUtility.SetDirty(item);
            }
            else 
            {
                if (item.UniquePassive != null)
                {
                    EditorGUILayout.LabelField(string.Format("Unique Passive: {0}", item.UniquePassive.Name));
                }
                if (item.StackablePassive != null)
                {
                    EditorGUILayout.LabelField(string.Format("Stackable Passive: {0}", item.StackablePassive.Name));
                }
                EditorGUILayout.LabelField(string.Format("{1} - {0}", item.PassiveDesc, item.PassiveName), GUILayout.ExpandHeight(true));
            }
            //EditorUtility.SetDirty(item);
            serializedObject.ApplyModifiedProperties();
        }

        private static bool ParentalCheck(Item parent, int childIndex)
        {
            var child = parent.BuildsInto[childIndex];
            if (child != null)
            {
                foreach (Item possibleParent in child.BuildsFrom)
                {
                    if (possibleParent == parent)
                        return true;
                }
            }
            return false;
        }


        private static void Deorphan(Item item)
        {
            // Add this item to the child list of its parent(s)
            foreach (var element in item.BuildsFrom)
            {
                if (element != null)
                {
                    List<Item> workingListOfChildren = new List<Item>();
                    workingListOfChildren.AddRange(element.BuildsInto);
                    // if we aren't in the child list of our parent, fix that. 
                    if (!workingListOfChildren.Contains(item))
                    {
                        workingListOfChildren.Add(item);
                        element.BuildsInto = workingListOfChildren.ToArray();
                        EditorUtility.SetDirty(element);
                    }
                }
            }   
        }

        private void StatInspector(UnitStats stats, UnitStats percentStats, List<StatType> whichStats)
        {
            foreach (StatType type in whichStats)
            {
                //EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel(type.ToString("G"));
                switch (type)
                {
                    case StatType.Armor:
                        stats.Armor = EditorGUILayout.FloatField(stats.Armor);
                        break;
                    case StatType.Haste:
                        stats.Haste = EditorGUILayout.FloatField(stats.Haste);
                        break;
                    case StatType.HealthRegen:
                        stats.HealthRegen = EditorGUILayout.FloatField(stats.HealthRegen);
                        break;
                    case StatType.Lifedrain:
                        stats.Lifedrain = EditorGUILayout.FloatField(stats.Lifedrain);
                        break;
                    case StatType.Mastery:
                        stats.Mastery = EditorGUILayout.FloatField(stats.Mastery);
                        break;
                    case StatType.MaxHealth:
                        stats.MaxHealth = EditorGUILayout.FloatField(stats.MaxHealth);
                        break;
                    case StatType.Penetration:
                        stats.Penetration = EditorGUILayout.FloatField(stats.Penetration);
                        break;
                    case StatType.PercentMovementSpeed:
                        percentStats.MovementSpeed = EditorGUILayout.FloatField(percentStats.MovementSpeed);
                        break;
                    case StatType.Power:
                        stats.Power = EditorGUILayout.FloatField(stats.Power);
                        break;
                    case StatType.Resistance:
                        stats.Resistance = EditorGUILayout.FloatField(stats.Resistance);
                        break;
                }
                //EditorGUILayout.EndHorizontal();

            }
        }

        private void StatLabelsInspector(UnitStats stats, UnitStats percentStats, List<StatType> whichStats)
        {
            foreach (StatType type in whichStats)
            {
                //EditorGUILayout.BeginHorizontal();
                var label = (type.ToString("G"));
                float value = 0;
                switch (type)
                {
                    case StatType.Armor:
                        value = stats.Armor;
                        break;
                    case StatType.Haste:
                        value = stats.Haste;
                        break;
                    case StatType.HealthRegen:
                        value = stats.HealthRegen;
                        break;
                    case StatType.Lifedrain:
                        value = stats.Lifedrain;
                        break;
                    case StatType.Mastery:
                        value = stats.Mastery;
                        break;
                    case StatType.MaxHealth:
                        value = stats.MaxHealth;
                        break;
                    case StatType.Penetration:
                        value = stats.Penetration;
                        break;
                    case StatType.PercentMovementSpeed:
                        value = percentStats.MovementSpeed;
                        break;
                    case StatType.Power:
                        value = stats.Power;
                        break;
                    case StatType.Resistance:
                        value = stats.Resistance;
                        break;
                }
                if (value > 0)
                {
                    EditorGUILayout.LabelField(string.Format("{0}: {1}", label, value));
                }

            }
        }
    }


#endif
}

