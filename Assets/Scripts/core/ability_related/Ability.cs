using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace core {

    public class Ability : ScriptableObject, IEquatable<Ability> {
        public string Name;

        public Sprite Icon;

        public ParticleIdentifier Particle;

        public Func<Ability, Unit, string> GetIconSubtext;                                          //caster, text out


        public static class UnitTargetShortcuts
        {
            public static bool EnemySimple(Ability ability, Unit caster, Unit target)
            {
                return caster.Team != target.Team;
            }
        }

        [Obsolete("Check the caster for ability levels using unit.GetAbilityLevelLessOne()")]
        public int Level = 1;


        public string BasicAttackAnimationOverride;
        public bool DeathBehaviourOverride = false; //MEME messy messy. :( Out damned spot!

        public string GetSubtextPrettified(Unit caster) {
            if (GetIconSubtext != null) {
                return GetIconSubtext(this, caster);
            }
            return "";
        }

        public float[] Cooldowns = new float[] {0f};


        public bool Equals(Ability other) {
            return Name == other.Name;
        }

        /// <summary>
        /// The stack value equation. 
        /// This can be used to get the effect value of any given passive or buff that stacks or doesn't stack.
        /// 
        /// The meaning of extremes:
        /// stackPercentEffectiveness = 0 --> Unique (no effect from additional stacks)
        /// stackPercentEffectiveness = 1 --> Every stack is 100% effective.
        /// </summary>
        /// <returns>The value based on number of stacks, effectiveness, and base value.</returns>
        /// <param name="baseValue">Base value.</param>
        /// <param name="stackPercentEffectiveness">Percent effectiveness of each stack past the first. between 1 and 0.</param>
        /// <param name="additionalStacks">How many additional stacks we have.</param>
        public float StackValue(float baseValue, float stackPercentEffectiveness, int additionalStacks) {
            if (additionalStacks <= -1)
            {
                Debug.LogErrorFormat("additionalStacks is {0}, but shouldn't be less than 0. " +
                    "This probably means the number of stacks of this passive is zero and it didn't get cleaned up.", additionalStacks);
                return 0f;
            }
            if (stackPercentEffectiveness == 0f || additionalStacks == 0)
                return baseValue;
            else
                return baseValue + (baseValue * stackPercentEffectiveness * additionalStacks); 
        } 
            
    }

    public static class CooldownPrettifier
    {
        public static string Prettify(this float value)
        {
            if (value <= 0)
            {
                return "";
            }
            else if (value < 10)
            {
                string cleanedCooldown = (Mathf.Round(value * 10) / 10f).ToString();
                if (cleanedCooldown.Length == 1)
                {
                    return cleanedCooldown + ".0";
                }
                else
                {
                    return cleanedCooldown;
                }
            }
            else
            {
                return Mathf.Round(value).ToString();
            }
        }

        public static float BonusPercent(this float percent)
        {
            return (100f + percent) / 100f;
        }


        public static float Percent(this float percent)
        {
            return (percent) / 100f;
        }

        public static string ToTitleCase(this string name)
        {
            var list = name.Split(' ');

            string result = "";

            foreach (string section in list)
            {
                if (section.Count() > 1)
                {
                    result += section.Substring(0, 1).ToUpper() + section.Substring(1);
                }

            }

            return result;
        }
    }

}
