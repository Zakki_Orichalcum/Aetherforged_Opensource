﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace core
{
    [Serializable]
    public class SkillRecord
    {
        public Castable skill;
        public int slot;
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SkillRecord))]
    public class SkillRecordDrawer : PropertyDrawer
    {

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            var spacer = position.height;

            float middlePosition = position.y + spacer + spacer / 2;
            float bottomPosition = position.y + spacer * 2f;


            var skillProperty = property.FindPropertyRelative("skill");
            var slotProperty = property.FindPropertyRelative("slot");

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);



            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // 
            float slotWidth = 16f;
            float labelWidth = 32f;
            float spacerWidth = 5f;
            float skillWidth = position.width - slotWidth - labelWidth - spacerWidth * 3;

            float labelX = position.x + skillWidth + spacerWidth * 2;
            float slotX = labelX + labelWidth + spacerWidth;


            // Calculate rects
            var skillRect = new Rect(position.x, position.y, skillWidth, spacer);
            var slotLabelRect = new Rect(labelX, position.y, labelWidth, spacer);
            var slotRect = new Rect(slotX, position.y, slotWidth, spacer);
            var specRect = new Rect(position.x + 170, position.y, 40, spacer);

            var colorRect = new Rect(position.x + 215, position.y, 50, spacer);


            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            EditorGUI.PropertyField(skillRect, property.FindPropertyRelative("skill"), GUIContent.none);
            EditorGUI.LabelField(slotLabelRect, "slot");
            EditorGUI.PropertyField(slotRect, property.FindPropertyRelative("slot"), GUIContent.none);
            //EditorGUI.PropertyField(specRect, property.FindPropertyRelative("spec"), GUIContent.none);

            //EditorGUI.PropertyField(colorRect, property.FindPropertyRelative("color"), GUIContent.none);


            //        EditorGUI.PropertyField(specsRect, property.FindPropertyRelative("extras"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
#endif
}
