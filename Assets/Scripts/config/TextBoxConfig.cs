﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace config {
    [DataContract]
    class TextBoxConfig {

        string name;

        [DataMember]
        string value;
        public string Value {
            get { return value; }
            set { this.value = value;
                inGameTextBox.GetComponent<InputField>().text = value;
                foreach (var action in callbacks) {
                    action(value);
                }
            }
        }


        GameObject inGameTextBox;

        public List<UnityAction<string>> callbacks;

        public TextBoxConfig(string name, string defaultValue, params UnityAction<string>[] actions) {
            this.name = name;
            this.value = defaultValue;


            callbacks = new List<UnityAction<string>>(actions);
        }

        public void SetUp(GameObject textbox) {
            this.inGameTextBox = textbox;

            var textboxComponent = inGameTextBox.GetComponent<InputField>();

            textboxComponent.text = Value;

            foreach (UnityAction<string> action in callbacks) {
                textboxComponent.onValueChanged.AddListener(action);
            }
            textboxComponent.onEndEdit.AddListener((s) => {
                value = s;
            });

            inGameTextBox.transform.Find("Label").GetComponent<Text>().text = name;
        }
//
//        public static string DescriptionAttr(InputField source) {
//            FieldInfo fi = source.GetType().GetField(source.ToString());
//
//            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
//                typeof(DescriptionAttribute), false);
//
//            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
//            else return source.ToString();
//        }

    }
}

