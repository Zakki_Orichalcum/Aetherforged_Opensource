﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using core;
using core.forger;

namespace ui
{
	public class ForgerHealthBar : MonoBehaviour
	{
        
        readonly float hpPerLine = 250;

		public float maxHealth;
		public float currentHealth;
		public Image healthBar;
		public Image damageBar;
		public Image shieldBar;

        public Image resourceBar;
		public Image expendBar;
        public Text nameText;

        public Text levelNumber;
		public float delay = 0.5f;
		public float decaySpeed = 0.5f;

        private GameObject linePrefab;

		private float delta;
        [SerializeField]
		private Unit thisForger;
		private float finalHealth;
		private float currentShield = 0.0f;

        private float damageValue;
		private float expendRatio;

        List<GameObject> lines = new List<GameObject>();

		void Start ()
		{
            thisForger = GetComponentInParent<Unit>();
            healthBar = transform.Find("Bars/HealthBar").GetComponent<Image>();
			damageBar = transform.Find("Bars/DamageBar").GetComponent<Image>();
            shieldBar = transform.Find("Bars/ShieldBar").GetComponent<Image>();


            if (thisForger.Kind == UnitKind.Forger) {
                levelNumber = transform.Find("Level/Level").GetComponent<Text>();
                resourceBar = transform.Find("Bars/Resource/ResourceBar").GetComponent<Image>();
				expendBar = transform.Find("Bars/Resource/ExpendBar").GetComponent<Image>();
                nameText = transform.Find("PlayerName").GetComponent<Text>();

                linePrefab = healthBar.transform.GetChild(0).gameObject;
                healthBar.transform.GetChild(0).gameObject.SetActive(false);
            }

            var noResource = thisForger.Resource == null || thisForger.Resource.MaxAmount == 0f;
            if (resourceBar != null && (resourceBar.transform.parent.gameObject.activeInHierarchy) == (noResource))
            {   // if we have a resource bar, and the resource bar group is active and our unit does not have a resource or vice-versa,
                // turn the resource bar group on or off to match the existence of the unit's resource
                resourceBar.transform.parent.gameObject.SetActive(!noResource);
            }

		}
		
		void Update ()
		{
            if(thisForger != null) {
                currentHealth = thisForger.Health;
                currentShield = thisForger.Shield;

                maxHealth = Mathf.Max (thisForger.MaxHealth, currentHealth + currentShield);

            }


            if (thisForger is Forger)
            {
                DrawLinesOverHealthBar();
            }

            healthBar.fillAmount = currentHealth / maxHealth;
            shieldBar.fillAmount = (currentShield + currentHealth) / maxHealth;



            if (resourceBar != null && thisForger.Resource != null)
            {
                var resource = thisForger.Resource;
                resourceBar.fillAmount = resource.Ratio;
                resourceBar.color = resource.Color;

                var resDelta = decaySpeed * Time.deltaTime;


                if (expendRatio < resource.Ratio)
                {
                    expendRatio = resource.Ratio;
                }
                else if (expendRatio > resource.Ratio)
                {
                    expendRatio -= resDelta;

                    //					StartCoroutine(ExpendResource(resource.Ratio));
                }

                expendBar.fillAmount = expendRatio;
            }
             


            if (levelNumber != null) {
                var level = thisForger.Level;
                levelNumber.text = string.Format("{0}", level);
            }

            if (nameText != null)
            {
                nameText.text = thisForger.Name;
            }

			if (currentHealth < finalHealth) {
				StopCoroutine("TakeDamage");
				StartCoroutine(TakeDamage(finalHealth));
			} 
//			else if (currentHealth == maxHealth) {
//				damageValue = currentHealth;
//				damageBar.fillAmount = damageValue / maxHealth;
//			}

			finalHealth = currentHealth;

		}

        [ContextMenu("Draw Lines Over HP bar")]
        void DrawLinesOverHealthBar () {
            if (linePrefab == null)
            {
                return;
            }

            var healthRectTransform = healthBar.GetComponent<RectTransform>();

            // this is the number of times hpPerLine divides into maxHealth
            var numlines = Mathf.FloorToInt(maxHealth / hpPerLine);

            // this is where the hp divided evenly so we use it to determine spacing of the lines
            var floorWidth = (numlines * hpPerLine) / maxHealth * healthRectTransform.rect.width;

            while (lines.Count() > 1 && numlines < lines.Count())
            {
                Destroy(lines[0]);
                lines.RemoveAt(0);
            }

            while(numlines-1 >= lines.Count())
            {
                var gob = Instantiate(linePrefab, healthBar.transform);
                gob.SetActive(true);
                //gob.transform.SetParent(healthBar.transform);
                lines.Add(gob);
            }


            for (int index = 1; index <= lines.Count(); ++index)
            {
                var rectTransform = lines[index - 1].GetComponent<RectTransform>();

                var offset = healthRectTransform.rect.width * -healthRectTransform.anchorMin.x;

                var position = rectTransform.anchoredPosition;
                position.x = index * (floorWidth / numlines) + offset;


                rectTransform.anchoredPosition = position;
            }
        }

		IEnumerator TakeDamage (float storedHealth)
		{
			if (damageValue < currentHealth)
			{
				damageValue = storedHealth;
			}

			yield return new WaitForSeconds (delay);

			while (damageValue > currentHealth)
			{
				delta = maxHealth * decaySpeed * Time.deltaTime;
				damageValue -= delta;
                damageBar.fillAmount = damageValue /  maxHealth;
				yield return null;
			}
		}

		IEnumerator ExpendResource (float targetResource)
		{
			var resDelta = decaySpeed * Time.deltaTime;

			if (expendRatio < targetResource)
			{
				expendRatio = targetResource;
			}

			yield return new WaitForSeconds (delay);

			while (expendRatio > targetResource)
			{
				expendRatio -= resDelta;
				expendBar.fillAmount = expendRatio;
				yield return null;
			}
		}

	}
}