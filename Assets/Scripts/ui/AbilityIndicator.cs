﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ui
{
	public class AbilityIndicator {

		private static GameObject SetupAbilityPlane(){
		//GameObject AbilityPlane = GameObject.CreatePrimitive (PrimitiveType.Plane);
		GameObject AbilityPlane = GameObject.Instantiate(Resources.Load("Images/IngameUI/AbilityIndicators/AbilityIndicatorPlane") as GameObject,new Vector3(0,0,0),Quaternion.identity);
		return AbilityPlane;
		}

		public static GameObject CreateAoEIndicator(Vector3 OriginPos, float radius, int shapeindex, bool followmouse){
			GameObject AbilityPlane = SetupAbilityPlane();
			if (shapeindex == 0 || shapeindex == null) {
				AbilityPlane.GetComponent<Renderer> ().material.mainTexture = Resources.Load ("Images/IngameUI/AbilityIndicators/normal_circle_360") as Texture;
			}
			if (shapeindex == 1) {
				AbilityPlane.GetComponent<Renderer> ().material.mainTexture = Resources.Load ("Images/IngameUI/AbilityIndicators/bevelled_circle_360") as Texture;
			}
			AbilityPlane.transform.localScale = new Vector3 (radius,1,radius);

			AbilityPlane.transform.position = OriginPos;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().maxrange = radius;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().aoeindicator = true;
			if (followmouse == true) {
				AbilityPlane.GetComponent<AbilityIndicatorHandler> ().targetmouse = true;
			}

			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().OriginPos = OriginPos;

			return AbilityPlane;

		}

		public static GameObject CreateLineIndicator(Vector3 OriginPos = default(Vector3),
			Vector3 TargetPos = default(Vector3), float minrange = default(float), float maxrange = default(float),
		float width = default(float), int shapeindex = default(int),
		bool followmouse = default(bool), bool followcaster = default(bool), Transform castertransform = default(Transform),
		bool followtarget = default(bool),
		Transform targettransform = default(Transform)){

			GameObject AbilityPlane = SetupAbilityPlane();
			if (shapeindex == 0 || shapeindex==null) {
				AbilityPlane.GetComponent<Renderer> ().material.mainTexture = Resources.Load ("Images/IngameUI/AbilityIndicators/LineIndicator") as Texture;
			}
			if (shapeindex == 1) {
				AbilityPlane.GetComponent<Renderer> ().material.mainTexture = Resources.Load ("Images/IngameUI/AbilityIndicators/CheeseWedgie") as Texture;
			}
			if (shapeindex == 2) {
				AbilityPlane.GetComponent<Renderer> ().material.mainTexture = Resources.Load ("Images/IngameUI/AbilityIndicators/LemonIndicator") as Texture;
			}
			if(maxrange==default(float)){
				maxrange = minrange;	
			}

			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().OriginPos = OriginPos;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().targetpos = TargetPos;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().minrange = minrange;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().maxrange = maxrange;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().width = width;
			AbilityPlane.GetComponent<AbilityIndicatorHandler> ().aoeindicator = false;
			if (followmouse == true) {
				AbilityPlane.GetComponent<AbilityIndicatorHandler> ().targetmouse = true;
			}
			if (followcaster == true) {
				AbilityPlane.GetComponent<AbilityIndicatorHandler> ().followcaster = true;
				AbilityPlane.GetComponent<AbilityIndicatorHandler> ().castertransform = castertransform;
				OriginPos = castertransform.position;
			}
			if (followtarget == true) {
				AbilityPlane.GetComponent<AbilityIndicatorHandler> ().followtarget = true;
				AbilityPlane.GetComponent<AbilityIndicatorHandler> ().targetransform = targettransform;
				TargetPos = targettransform.position;
			}

			AbilityPlane.transform.localScale = new Vector3 (width,1,maxrange);

			//Debug.DrawLine (CasterPos,TargetPos,Color.green,100f); Debug to find the goddamn position
			AbilityPlane.transform.position = OriginPos;
			AbilityPlane.transform.LookAt (TargetPos);
			AbilityPlane.transform.Translate (Vector3.forward*(maxrange*5));

			return AbilityPlane;
		}

		private static float range = 0f;

		public static void HandleLineIndicator(GameObject indicator, Vector3 OriginPos = default(Vector3), Vector3 TargetPos = default(Vector3), float minrange = 0f, float maxrange = 0f, float width = 0f){
			//Debug.DrawLine (CasterPos,TargetPos,Color.green,1f); Debug to find the goddamn position
			indicator.transform.position = OriginPos;
			indicator.transform.LookAt (TargetPos);

			if (minrange != maxrange) {
				if (((maxrange * Vector3.Distance (OriginPos, TargetPos)) / 10) > minrange && ((maxrange * Vector3.Distance (OriginPos, TargetPos)) / 10) < maxrange) {
					range = ((maxrange * Vector3.Distance (OriginPos, TargetPos)) / 10);
				}

				indicator.transform.localScale = new Vector3 (width,1,range);
				indicator.transform.Translate (Vector3.forward * (range * 5));
			} 
			else {
				indicator.transform.Translate (Vector3.forward * (maxrange * 5));
			}
		}
			
}
}