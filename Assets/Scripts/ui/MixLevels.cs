﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MixLevels : MonoBehaviour {

	public AudioMixer masterMixer;

	public void SetMusicVolume(float MusicVol){
		masterMixer.SetFloat("Music Volume", MusicVol);
	}
	public void SetEffectsVolume(float EffectVol){
		masterMixer.SetFloat("SFX Volume", EffectVol);
	}
	public void SetVoiceVolume(float VoiceVol){
		masterMixer.SetFloat("Voice Volume", VoiceVol);
	}
	public void SetMasterVolume(float MasterVol){
		masterMixer.SetFloat("Master Volume", MasterVol);
	}
	public void SetAmbienceVolume(float AmbienceVol){
		masterMixer.SetFloat("Ambience Volume", AmbienceVol);
	}	
	public void SetAnnouncerVolume(float AnnouncerVol){
		masterMixer.SetFloat("Announcer Volume", AnnouncerVol);
	}				
}
