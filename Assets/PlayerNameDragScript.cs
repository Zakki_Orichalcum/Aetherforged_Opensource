﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerInPrivateLobbyModel {
    public string Username { get; set; }
}

public class PlayerNameDragScript : MonoBehaviour {
    Vector3 draggingObjOriginalPosition;
    GameObject draggingObjOriginalParent;
    GameObject draggingObj = null;

    PlayerInPrivateLobbyModel[] TeamLeft = new PlayerInPrivateLobbyModel[5];
    PlayerInPrivateLobbyModel[] TeamRight = new PlayerInPrivateLobbyModel[5];
    PlayerInPrivateLobbyModel[] Unassigned = new PlayerInPrivateLobbyModel[50];

    public void Awake() {
        TeamLeft = new PlayerInPrivateLobbyModel[5];
        TeamRight = new PlayerInPrivateLobbyModel[5];
        Unassigned = new PlayerInPrivateLobbyModel[50];
        RebuildPanels();
    }

    public void SetLocalPlayerToSlot(int slot) {
        var localPlayerModel = new PlayerInPrivateLobbyModel() {
            Username = NetworkStore.Username,
        };
        for(int i = 0; i < 5; i++) {
            if (TeamLeft[i] != null && TeamLeft[i].Username == localPlayerModel.Username) {
                TeamLeft[i] = null;
            }
            if (TeamRight[i] != null && TeamRight[i].Username == localPlayerModel.Username) {
                TeamRight[i] = null;
            }
        }
        if (slot <= 4) {
            TeamLeft[slot] = localPlayerModel;
        } else {
            TeamRight[slot - 5] = localPlayerModel;
        }
        RebuildPanels();
    }

    private void RebuildPanels() {
        var LeftPlayerPanelContainer = transform.GetChild(0).GetChild(1);
        var RightPlayerPanelContainer = transform.GetChild(1).GetChild(1);
        RebuildTeamPanel(LeftPlayerPanelContainer, TeamLeft);
        RebuildTeamPanel(RightPlayerPanelContainer, TeamRight);
    }

    private void RebuildTeamPanel(Transform playerPanelContainer, PlayerInPrivateLobbyModel[] team) {
        for (int i = 0; i < 5; i++) {
            var playerPanel = playerPanelContainer.GetChild(i).GetChild(0);
            if (team[i] == null) {
                playerPanel.gameObject.GetComponent<Image>().color = new Color(0, 0, 0, 0);
                playerPanel.GetChild(0).GetComponent<Text>().text = "";
                playerPanel.GetChild(1).gameObject.SetActive(true);
            } else {
                playerPanel.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                playerPanel.GetChild(0).GetComponent<Text>().text = team[i].Username;
                playerPanel.GetChild(1).gameObject.SetActive(false);
            }
        }
    }

    public void OnStartDrag(GameObject obj) {
        if (draggingObj == null) {
            draggingObj = obj;
            draggingObjOriginalPosition = draggingObj.GetComponent<RectTransform>().position;
            draggingObjOriginalParent = draggingObj.transform.parent.gameObject;
            draggingObj.transform.SetParent(transform);
            draggingObj.transform.SetAsLastSibling();
        }
    }

    public void OnDrag(BaseEventData eventData) {
        var pointerData = eventData as PointerEventData;

        if (pointerData.dragging) {
            var rect = draggingObj.GetComponent<RectTransform>();
            var currentPosition = rect.position;
            currentPosition.x += pointerData.delta.x / 4;
            currentPosition.y += pointerData.delta.y / 4;
            rect.position = currentPosition;
        } else {
            print("Not");
        }

        print(draggingObj.name);
    }

    public void OnEndDrag(BaseEventData eventData) {
        print("Drag Delta: " + (draggingObj.GetComponent<RectTransform>().position - draggingObjOriginalPosition));
        draggingObj.transform.SetParent(draggingObjOriginalParent.transform);
        draggingObj.GetComponent<RectTransform>().position = draggingObjOriginalPosition;
        draggingObjOriginalParent = null;
        draggingObjOriginalPosition = Vector3.zero;
        draggingObj = null;
    }
}
